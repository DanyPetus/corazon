import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-tabres',
  templateUrl: './tabres.component.html',
  styleUrls: ['./tabres.component.scss'],
})
export class TabresComponent implements OnInit {

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
    console.log(this.router.url)
  }

  goGroups(){
    this.router.navigate(['/groups']);
  }

  goWeekly(){
    this.router.navigate(['/weekly']);
  }

  goHome(){
    this.router.navigate(['/home']);
  }

  goResources(){
    this.router.navigate(['/resources']);
  }

  goSos(){
    this.router.navigate(['/sos']);
  }

}
