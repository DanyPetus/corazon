import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-tabuser',
  templateUrl: './tabuser.component.html',
  styleUrls: ['./tabuser.component.scss'],
})
export class TabuserComponent implements OnInit {

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {

  }

  goGroups(){
    this.router.navigate(['/groups']);
  }

  goWeekly(){
    this.router.navigate(['/weekly']);
  }

  goHome(){
    this.router.navigate(['/home']);
  }

  goResources(){
    this.router.navigate(['/resources']);
  }

  goSos(){
    this.router.navigate(['/sos']);
  }

}
