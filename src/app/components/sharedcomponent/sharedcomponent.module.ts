import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TabuserComponent } from '../tabuser/tabuser.component'
import { MenuserComponent } from '../menuser/menuser.component'
import { TabgroupComponent } from '../tabgroup/tabgroup.component'
import { TabresComponent } from '../tabres/tabres.component'
import { TabweekComponent } from '../tabweek/tabweek.component'
import { TabsosComponent } from '../tabsos/tabsos.component'

@NgModule({
  declarations: [TabuserComponent, MenuserComponent, TabgroupComponent, TabresComponent, TabweekComponent, TabsosComponent],
  imports: [ 
    FormsModule,  
    CommonModule,
    IonicModule
  ],
  exports: [TabuserComponent, MenuserComponent, TabgroupComponent, TabresComponent, TabweekComponent, TabsosComponent]
})
export class SharedComponentsModule { }