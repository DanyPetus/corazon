import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-menuser',
  templateUrl: './menuser.component.html',
  styleUrls: ['./menuser.component.scss'],
})
export class MenuserComponent implements OnInit {

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {}

  goProfile(){
    this.router.navigate(['/profile']);
  }

  goDonation(){
    this.router.navigate(['/donation']);
  }

  goContact(){
    this.router.navigate(['/contact']);
  }

  goOut(){
    this.router.navigate(['/intro']);
  }

}
