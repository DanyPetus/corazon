import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.page.html',
  styleUrls: ['./videos.page.scss'],
})
export class VideosPage implements OnInit {

  constructor(
    private router:Router
  ) { }

  ngOnInit() {
  }

  goVideo(){
    this.router.navigate(["videos1"])
  }

}
