import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Videos1Page } from './videos1.page';

const routes: Routes = [
  {
    path: '',
    component: Videos1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Videos1PageRoutingModule {}
