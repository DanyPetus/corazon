import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Videos1PageRoutingModule } from './videos1-routing.module';

import { Videos1Page } from './videos1.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    IonicModule,
    Videos1PageRoutingModule
  ],
  declarations: [Videos1Page]
})
export class Videos1PageModule {}
