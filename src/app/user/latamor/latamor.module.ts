import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LatamorPageRoutingModule } from './latamor-routing.module';

import { LatamorPage } from './latamor.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedComponentsModule,
    IonicModule,
    LatamorPageRoutingModule
  ],
  declarations: [LatamorPage]
})
export class LatamorPageModule {}
