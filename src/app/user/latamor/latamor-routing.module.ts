import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LatamorPage } from './latamor.page';

const routes: Routes = [
  {
    path: '',
    component: LatamorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LatamorPageRoutingModule {}
