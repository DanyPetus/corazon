import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.page.html',
  styleUrls: ['./resources.page.scss'],
})
export class ResourcesPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    slidesPerView: 1.2,
    grabCursor: true,
    freeMode: true
  };

  images: any;
  imgPodcast: any;
  imgVideo: any;
  imgMusica: any;
  imgEstudios: any;

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private firestoreService: FirestoreService,
  ) {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(true, 'first');
  }

  goWeekly() {
    this.router.navigate(['/weekly']);
  }

  goVideo() {
    this.router.navigate(['/videos']);
  }

  goPodcast() {
    this.router.navigate(['/podcast']);
  }

  goEstudios() {
    this.router.navigate(["estudios"])
  }

  goSerie(){
    this.router.navigate(["series"])
  }
  ngOnInit() {
    this.getImg();
  }

  getImg() {
    this.firestoreService.getImgs().subscribe((images) => {
      this.images = images;
      console.log(this.images);
      this.imgPodcast = this.images[2].file;
      this.imgVideo = this.images[3].file;
      this.imgMusica = this.images[1].file;
      this.imgEstudios = this.images[0].file;
    });
  }


}
