import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sos2',
  templateUrl: './sos2.page.html',
  styleUrls: ['./sos2.page.scss'],
})
export class Sos2Page implements OnInit {

  viewArt : boolean = true;

  constructor() { }

  ngOnInit() {
  }

  send(){
    this.viewArt = false;
  }

}
