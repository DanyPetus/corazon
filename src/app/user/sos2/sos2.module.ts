import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Sos2PageRoutingModule } from './sos2-routing.module';

import { Sos2Page } from './sos2.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedComponentsModule,
    IonicModule,
    Sos2PageRoutingModule
  ],
  declarations: [Sos2Page]
})
export class Sos2PageModule {}
