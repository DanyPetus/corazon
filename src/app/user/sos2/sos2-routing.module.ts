import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Sos2Page } from './sos2.page';

const routes: Routes = [
  {
    path: '',
    component: Sos2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Sos2PageRoutingModule {}
