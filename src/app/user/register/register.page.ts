import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import {
  Storage
} from '@ionic/storage';
// SERVICES
import { UtilsService } from 'src/app/services/utils/utils.service';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  email : string;
  password : string;
  nombre : string;
  apellido : string;
  telefono : string;
  fecha : any;
  genero : any;

  check : boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private utilsService: UtilsService,
    private firestoreService: FirestoreService,
    private storage: Storage,
  ) { }

  ngOnInit() {
  }

  goBack() {
    this.router.navigate(['/intro']);
  }

  goToLogin() {
    this.router.navigate(['/login'])
  }

  createUserAuth() {
    this.firestoreService.createUserAuth(this.email)
      .then((userAuth) => {
        const {
          user
        } = userAuth;
        this.goRegister(user.uid);
      }).catch((error: any) => {
        console.log(error);

      });
  }

  async goRegister(uidAuth:any) {

    const userInfo = {
      uid: uidAuth,
      firstName: this.nombre,
      lastName: this.apellido,
      email: this.email,
      gender: this.genero,
      phone: this.telefono,
      birthdate: this.fecha,
      roleName: 'Cliente',
      roleType: 3,
      imgProfile: '',
      tokenNotifications: '',
      typeRegister: 1,
      services: 0,
      status: 1,
      login: 0,
      createBy: "app",
      createDate: new Date(),
      updateBy: "app",
      updateDate: new Date()
    }
    console.log(userInfo);

    this.firestoreService.createUser(userInfo, uidAuth).then(() => {
      this.storage.set('user', userInfo);
      this.router.navigate(['/home'])
      // this.successAlert('Usuario Guardado', 'El usuario fue guardado con exito.');
    });
  }

  selectFecha(ev){
    this.fecha = new Date(ev.detail.value);
    console.log(this.fecha);
  }

  gender(ev){
    console.log(ev.detail.value);
    this.genero = ev.detail.value;
  }

  terms(ev){
    console.log(ev.detail.checked);
    this.check = ev.detail.checked;
  }
}
