import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import {
  Storage
} from '@ionic/storage';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email : string;
  password : string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private firestoreService: FirestoreService,
    private storage: Storage,
  ) { }

  ngOnInit() {
  }

  goBack() {
    this.navCtrl.back();
  }

  goToRegister() {
    this.router.navigate(['/register']);
  }

  handleLogin() {
    this.firestoreService.login(this.email, this.password)
      .then((user) => {
        this.handleCookieAsigned(this.email);
      }).catch((error: any) => {
        // this.errorAlert('Error Login', error.message);
      });
  }

  async handleCookieAsigned(email) {
    await this.firestoreService.getUser(email).then(info => {
      info.docs.map(doc => {
        const userData = doc.data();
        console.log(userData);
        this.storage.set('user', userData);
        this.router.navigate(['/home']);
      });
    });
  }

}
