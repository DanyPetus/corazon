import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';

@Component({
  selector: 'app-groupsdetail',
  templateUrl: './groupsdetail.page.html',
  styleUrls: ['./groupsdetail.page.scss'],
})
export class GroupsdetailPage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    slidesPerView: 1.5,
    grabCursor: true,
    freeMode: true
  };

  data: any;
  date : any;

  constructor(
    private route: ActivatedRoute, private router: Router,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        this.data = JSON.parse(params.dataCard);
        console.log(this.data);
      }
      var time = new Date(this.data.createDate.seconds*1000 + this.data.createDate.nanoseconds/100000);
      this.date = time.getDate() + "/" + (time.getMonth() + 1) + "/" + time.getFullYear();
    });
  }

}
