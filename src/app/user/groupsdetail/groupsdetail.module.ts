import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupsdetailPageRoutingModule } from './groupsdetail-routing.module';

import { GroupsdetailPage } from './groupsdetail.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupsdetailPageRoutingModule,
    SharedComponentsModule
  ],
  declarations: [GroupsdetailPage]
})
export class GroupsdetailPageModule {}
