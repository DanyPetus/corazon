import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupsdetailPage } from './groupsdetail.page';

const routes: Routes = [
  {
    path: '',
    component: GroupsdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupsdetailPageRoutingModule {}
