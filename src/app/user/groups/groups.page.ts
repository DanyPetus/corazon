import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
// Services
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.page.html',
  styleUrls: ['./groups.page.scss'],
})
export class GroupsPage implements OnInit {
  grupos: any;

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private firestoreService: FirestoreService
  ) {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(true, 'first');
  }

  goWeekly() {
    this.router.navigate(['/weekly']);
  }

  ngOnInit() {
    this.getUsers();
  }

  async getUsers() {
    this.firestoreService.getGrupo().subscribe((groups) => {
      this.grupos = groups;
      console.log(this.grupos)
    });
  }

  goNext(grupo) {
    // Data que enviaremos
    let data = grupo
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['groupsdetail'], navigationExtras);
  }

}
