import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LatadoranPageRoutingModule } from './latadoran-routing.module';

import { LatadoranPage } from './latadoran.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedComponentsModule,
    LatadoranPageRoutingModule
  ],
  declarations: [LatadoranPage]
})
export class LatadoranPageModule {}
