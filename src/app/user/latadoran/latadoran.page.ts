import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-latadoran',
  templateUrl: './latadoran.page.html',
  styleUrls: ['./latadoran.page.scss'],
})
export class LatadoranPage implements OnInit {

  rojo1 : boolean;
  amarillo1 : boolean;
  verde1 : boolean;

  rojo2 : boolean;
  amarillo2 : boolean;
  verde2 : boolean;

  rojo3 : boolean;
  amarillo3 : boolean;
  verde3 : boolean;

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
  ) {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(true, 'first');
  }

  ngOnInit() {

      
  }

  change1(ev){
    let value = ev.detail.value;

    if (value < 33){
      this.rojo1 = true;
      this.amarillo1 = false;
      this.verde1 = false;
    } else if (value >= 33 && value < 66){
      this.rojo1 = false;
      this.amarillo1 = true;
      this.verde1 = false;
    } else if (value >= 66){
      this.rojo1 = false;
      this.amarillo1 = false;
      this.verde1 = true;
    }
  }

  change2(ev){
    let value = ev.detail.value;

    if (value < 33){
      this.rojo2 = true;
      this.amarillo2 = false;
      this.verde2 = false;
    } else if (value >= 33 && value < 66){
      this.rojo2 = false;
      this.amarillo2 = true;
      this.verde2 = false;
    } else if (value >= 66){
      this.rojo2 = false;
      this.amarillo2 = false;
      this.verde2 = true;
    }
  }

  change3(ev){
    let value = ev.detail.value;

    if (value < 33){
      this.rojo3 = true;
      this.amarillo3 = false;
      this.verde3 = false;
    } else if (value >= 33 && value < 66){
      this.rojo3 = false;
      this.amarillo3 = true;
      this.verde3 = false;
    } else if (value >= 66){
      this.rojo3 = false;
      this.amarillo3 = false;
      this.verde3 = true;
    }
  }

}
