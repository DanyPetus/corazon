import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LatadoranPage } from './latadoran.page';

const routes: Routes = [
  {
    path: '',
    component: LatadoranPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LatadoranPageRoutingModule {}
