import {
  Component,
  OnInit
} from '@angular/core';
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';

@Component({
  selector: 'app-podcast',
  templateUrl: './podcast.page.html',
  styleUrls: ['./podcast.page.scss'],
})
export class PodcastPage implements OnInit {
  podcasts: any;

  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
  ) {}

  ngOnInit() {
    this.getPodcast();
  }

  goPodcast(podcast) {
    // Data que enviaremos
    let data = podcast
    let navigationExtras: NavigationExtras = {
      queryParams: {
        // dataCard: Nombre que indicamos que sera recibido en formularioinformacion
        // JSON.stringify(data): Recibe data y la convierte en un JSON
        dataCard: JSON.stringify(data)
      }
    };
    // Ruta de Page, NavigationExtras: Data a enviar
    this.router.navigate(['podcasts'], navigationExtras);
  }

  getPodcast() {
    this.firestoreService.getPodcastsAll().subscribe((podcasts) => {
      this.podcasts = podcasts;
      console.log(this.podcasts)
    });
  }

}
