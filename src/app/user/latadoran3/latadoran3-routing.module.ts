import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Latadoran3Page } from './latadoran3.page';

const routes: Routes = [
  {
    path: '',
    component: Latadoran3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Latadoran3PageRoutingModule {}
