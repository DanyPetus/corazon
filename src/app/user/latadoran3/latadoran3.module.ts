import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Latadoran3PageRoutingModule } from './latadoran3-routing.module';

import { Latadoran3Page } from './latadoran3.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedComponentsModule,
    Latadoran3PageRoutingModule
  ],
  declarations: [Latadoran3Page]
})
export class Latadoran3PageModule {}
