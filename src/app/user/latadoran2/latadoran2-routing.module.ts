import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Latadoran2Page } from './latadoran2.page';

const routes: Routes = [
  {
    path: '',
    component: Latadoran2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Latadoran2PageRoutingModule {}
