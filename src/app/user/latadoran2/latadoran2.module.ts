import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Latadoran2PageRoutingModule } from './latadoran2-routing.module';

import { Latadoran2Page } from './latadoran2.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedComponentsModule,
    Latadoran2PageRoutingModule
  ],
  declarations: [Latadoran2Page]
})
export class Latadoran2PageModule {}
