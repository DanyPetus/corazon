import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-latadoran2',
  templateUrl: './latadoran2.page.html',
  styleUrls: ['./latadoran2.page.scss'],
})
export class Latadoran2Page implements OnInit {

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
  ) {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(true, 'first');
  }

  ngOnInit() {

      
  }

}
