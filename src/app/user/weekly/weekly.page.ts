import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-weekly',
  templateUrl: './weekly.page.html',
  styleUrls: ['./weekly.page.scss'],
})
export class WeeklyPage implements OnInit {
  acordionExapanded = false;
  @ViewChild("cc", {
    read: true,
    static: false
  }) CardContent: any;
  i: any

  weeklys : any;

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    private firestoreService: FirestoreService,
  ) {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(true, 'first');
  }

  goWeekly() {
    this.router.navigate(['/weekly']);
  }

  ngOnInit() {

    let accItem = document.getElementsByClassName('accordionItem');
    let accHD = document.getElementsByClassName('accordionItemHeading');
    for (this.i = 0; this.i < accHD.length; this.i++) {
      accHD[this.i].addEventListener('click', toggleItem, false);
    }

    function toggleItem() {
      let itemClass = this.parentNode.className;
      for (this.i = 0; this.i < accItem.length; this.i++) {
        accItem[this.i].className = 'accordionItem close';
      }
      if (itemClass == 'accordionItem close') {
        this.parentNode.className = 'accordionItem open';
      }
    }

    this.getEstudiosUser();
  }

  getEstudiosUser() {
    this.firestoreService.getEstudiosSem().subscribe((weekly) => {
      this.weeklys = weekly;
      console.log(this.weeklys);
    });
  }

}
