import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Latadoran4Page } from './latadoran4.page';

const routes: Routes = [
  {
    path: '',
    component: Latadoran4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Latadoran4PageRoutingModule {}
