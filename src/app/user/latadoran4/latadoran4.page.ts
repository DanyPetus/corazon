import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';
import * as countdown from 'countdown'

interface Time {
  days : number,
  hours: number,
  minutes : number,
  seconds : number
}

@Component({
  selector: 'app-latadoran4',
  templateUrl: './latadoran4.page.html',
  styleUrls: ['./latadoran4.page.scss'],
})
export class Latadoran4Page implements OnInit {
  time : Time = null;
  timerID : number = null;

  dias: any;
  horas: any;
  minutos: any;

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
  ) {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(true, 'first');
  }

  ngOnInit() {
    let date = new Date("2021-06-06")

    this.timerID = countdown(date, (ts)=>{
      this.time = ts;
    }), countdown.DAYS | countdown.HOURS | countdown.MINUTES | countdown.SECONDS
  }

  ngOnDestroy(){
    if (this.timerID){
      clearInterval(this.timerID)
    }
  }

}
