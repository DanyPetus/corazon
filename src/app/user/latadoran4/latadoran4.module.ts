import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Latadoran4PageRoutingModule } from './latadoran4-routing.module';

import { Latadoran4Page } from './latadoran4.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedComponentsModule,
    Latadoran4PageRoutingModule
  ],
  declarations: [Latadoran4Page]
})
export class Latadoran4PageModule {}
