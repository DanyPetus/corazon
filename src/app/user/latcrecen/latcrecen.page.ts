import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-latcrecen',
  templateUrl: './latcrecen.page.html',
  styleUrls: ['./latcrecen.page.scss'],
})
export class LatcrecenPage implements OnInit {

  acordionExapanded = false;
  @ViewChild("cc", {
    read: true,
    static: false
  }) CardContent: any;
  i: any

  posicionp = "semana";

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
  ) {
    this.openFirst();
  }

  openFirst() {
    this.menu.enable(true, 'first');
  }

  goWeekly() {
    this.router.navigate(['/weekly']);
  }

  ngOnInit() {

    let accItem = document.getElementsByClassName('accordionItem');
    let accHD = document.getElementsByClassName('accordionItemHeading');
    for (this.i = 0; this.i < accHD.length; this.i++) {
      accHD[this.i].addEventListener('click', toggleItem, false);
    }

    function toggleItem() {
      let itemClass = this.parentNode.className;
      for (this.i = 0; this.i < accItem.length; this.i++) {
        accItem[this.i].className = 'accordionItem close';
      }
      if (itemClass == 'accordionItem close') {
        this.parentNode.className = 'accordionItem open';
      }
    }

  }

}
