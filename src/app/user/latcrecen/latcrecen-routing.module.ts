import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LatcrecenPage } from './latcrecen.page';

const routes: Routes = [
  {
    path: '',
    component: LatcrecenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LatcrecenPageRoutingModule {}
