import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LatcrecenPageRoutingModule } from './latcrecen-routing.module';

import { LatcrecenPage } from './latcrecen.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedComponentsModule,
    IonicModule,
    LatcrecenPageRoutingModule
  ],
  declarations: [LatcrecenPage]
})
export class LatcrecenPageModule {}
