import { Component, OnInit } from '@angular/core';
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-series',
  templateUrl: './series.page.html',
  styleUrls: ['./series.page.scss'],
})
export class SeriesPage implements OnInit {
  images : any;
  imgVideo : any;

  constructor(
    private firestoreService: FirestoreService,
  ) { }

  ngOnInit() {
    this.getImg();
  }

  getImg(){
    this.firestoreService.getImgs().subscribe((images) => {
      this.images = images;
      console.log(this.images);
      this.imgVideo = this.images[3].file;
    });
  }

}
