import { Component, OnInit } from '@angular/core';
import {
  Router,
  NavigationExtras,
  ActivatedRoute
} from '@angular/router';
import {
  NavController,
  Platform,
  AlertController,
  LoadingController,
  MenuController
} from '@ionic/angular';

@Component({
  selector: 'app-sos',
  templateUrl: './sos.page.html',
  styleUrls: ['./sos.page.scss'],
})
export class SosPage implements OnInit {

  constructor(
    private menu: MenuController,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    ) {
      this.openFirst();
    }
  
    openFirst() {
      this.menu.enable(true, 'first');
    }
  
    goWeekly(){
      this.router.navigate(['/weekly']);
    }

  ngOnInit() {
  }

}
