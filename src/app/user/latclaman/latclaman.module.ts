import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LatclamanPageRoutingModule } from './latclaman-routing.module';

import { LatclamanPage } from './latclaman.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedComponentsModule,
    IonicModule,
    LatclamanPageRoutingModule
  ],
  declarations: [LatclamanPage]
})
export class LatclamanPageModule {}
