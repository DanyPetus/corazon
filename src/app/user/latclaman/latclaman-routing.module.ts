import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LatclamanPage } from './latclaman.page';

const routes: Routes = [
  {
    path: '',
    component: LatclamanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LatclamanPageRoutingModule {}
