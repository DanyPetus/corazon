import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgradecimientoPageRoutingModule } from './agradecimiento-routing.module';

import { AgradecimientoPage } from './agradecimiento.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    SharedComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    AgradecimientoPageRoutingModule
  ],
  declarations: [AgradecimientoPage]
})
export class AgradecimientoPageModule {}
