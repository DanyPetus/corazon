import {
  Component,
  OnInit
} from '@angular/core';
import {
  FirestoreService
} from 'src/app/services/firestore/firestore.service';
import {
  ActivatedRoute,
  Router,
  NavigationExtras
} from '@angular/router';

@Component({
  selector: 'app-podcasts',
  templateUrl: './podcasts.page.html',
  styleUrls: ['./podcasts.page.scss'],
})
export class PodcastsPage implements OnInit {
  images: any;
  imgPodcast: any;
  data: any;
  length: any;

  files = [];

  constructor(
    private firestoreService: FirestoreService,
    private route: ActivatedRoute, private router: Router,
  ) {}

  ngOnInit() {
    // Route recibe los parametros (se debe importar)
    this.route.queryParams.subscribe(params => {
      // Valida que exista params y params.dataCard que son los datos que recibiremos
      if (params && params.dataCard) {
        // Los parseamos nuevamente a objeto y podemos acceder a sus propiedades
        let data = JSON.parse(params.dataCard);
        this.data = data;
        this.length = this.data.files.length - 1;
        this.files = this.data.files;
        const index = this.files.indexOf(0);
        this.files.splice(index, 1);
        
        console.log(this.files);
        this.getImg();
      }
    });
  }

  getImg() {
    this.firestoreService.getImgs().subscribe((images) => {
      this.images = images;
      console.log(this.images);
      this.imgPodcast = this.images[2].file;
    });
  }

}
