import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PodcastsPageRoutingModule } from './podcasts-routing.module';

import { PodcastsPage } from './podcasts.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    SharedComponentsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    PodcastsPageRoutingModule
  ],
  declarations: [PodcastsPage]
})
export class PodcastsPageModule {}
