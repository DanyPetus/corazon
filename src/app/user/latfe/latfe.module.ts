import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LatfePageRoutingModule } from './latfe-routing.module';

import { LatfePage } from './latfe.page';
import { SharedComponentsModule } from 'src/app/components/sharedcomponent/sharedcomponent.module';

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    FormsModule,
    IonicModule,
    LatfePageRoutingModule
  ],
  declarations: [LatfePage]
})
export class LatfePageModule {}
