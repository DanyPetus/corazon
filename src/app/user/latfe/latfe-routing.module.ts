import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LatfePage } from './latfe.page';

const routes: Routes = [
  {
    path: '',
    component: LatfePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LatfePageRoutingModule {}
