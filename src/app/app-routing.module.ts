import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./user/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'intro',
    pathMatch: 'full'
  },
  {
    path: 'intro',
    loadChildren: () => import('./user/intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./user/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./user/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./user/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'donation',
    loadChildren: () => import('./user/donation/donation.module').then( m => m.DonationPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./user/contact/contact.module').then( m => m.ContactPageModule)
  },
  {
    path: 'groups',
    loadChildren: () => import('./user/groups/groups.module').then( m => m.GroupsPageModule)
  },
  {
    path: 'weekly',
    loadChildren: () => import('./user/weekly/weekly.module').then( m => m.WeeklyPageModule)
  },
  {
    path: 'resources',
    loadChildren: () => import('./user/resources/resources.module').then( m => m.ResourcesPageModule)
  },
  {
    path: 'sos',
    loadChildren: () => import('./user/sos/sos.module').then( m => m.SosPageModule)
  },
  {
    path: 'videos',
    loadChildren: () => import('./user/videos/videos.module').then( m => m.VideosPageModule)
  },
  {
    path: 'podcast',
    loadChildren: () => import('./user/podcast/podcast.module').then( m => m.PodcastPageModule)
  },
  {
    path: 'latclaman',
    loadChildren: () => import('./user/latclaman/latclaman.module').then( m => m.LatclamanPageModule)
  },
  {
    path: 'latfe',
    loadChildren: () => import('./user/latfe/latfe.module').then( m => m.LatfePageModule)
  },
  {
    path: 'latcrecen',
    loadChildren: () => import('./user/latcrecen/latcrecen.module').then( m => m.LatcrecenPageModule)
  },
  {
    path: 'latamor',
    loadChildren: () => import('./user/latamor/latamor.module').then( m => m.LatamorPageModule)
  },
  {
    path: 'latadoran',
    loadChildren: () => import('./user/latadoran/latadoran.module').then( m => m.LatadoranPageModule)
  },
  {
    path: 'latadoran2',
    loadChildren: () => import('./user/latadoran2/latadoran2.module').then( m => m.Latadoran2PageModule)
  },
  {
    path: 'latadoran3',
    loadChildren: () => import('./user/latadoran3/latadoran3.module').then( m => m.Latadoran3PageModule)
  },
  {
    path: 'latadoran4',
    loadChildren: () => import('./user/latadoran4/latadoran4.module').then( m => m.Latadoran4PageModule)
  },
  {
    path: 'sos2',
    loadChildren: () => import('./user/sos2/sos2.module').then( m => m.Sos2PageModule)
  },
  
  {
    path: 'podcasts',
    loadChildren: () => import('./user/podcasts/podcasts.module').then( m => m.PodcastsPageModule)
  },
  {
    path: 'estudios',
    loadChildren: () => import('./user/estudios/estudios.module').then( m => m.EstudiosPageModule)
  },
  {
    path: 'pago',
    loadChildren: () => import('./user/pago/pago.module').then( m => m.PagoPageModule)
  },
  {
    path: 'agradecimiento',
    loadChildren: () => import('./user/agradecimiento/agradecimiento.module').then( m => m.AgradecimientoPageModule)
  },
  {
    path: 'groupsdetail',
    loadChildren: () => import('./user/groupsdetail/groupsdetail.module').then( m => m.GroupsdetailPageModule)
  },
  {
    path: 'videos1',
    loadChildren: () => import('./user/videos1/videos1.module').then( m => m.Videos1PageModule)
  },
  {
    path: 'series',
    loadChildren: () => import('./user/series/series.module').then( m => m.SeriesPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
