import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from  "@angular/fire/auth";

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  gblAssetsCollection: AngularFirestoreCollection;
  gblUserCollection: AngularFirestoreCollection;
  gblGrupoCollection: AngularFirestoreCollection;
  gblSemanalCollection: AngularFirestoreCollection;
  gblPodcastCollection: AngularFirestoreCollection;

  constructor(
    private afAuth: AngularFireAuth,
    private afStore: AngularFirestore
  ) { 
    this.gblGrupoCollection = this.afStore.collection('gbl_Grupo');
    this.gblAssetsCollection = this.afStore.collection('gbl_imgs');
    this.gblUserCollection = this.afStore.collection('gbl_user');
    this.gblSemanalCollection = this.afStore.collection('gbl_semanal');
    this.gblPodcastCollection = this.afStore.collection('gbl_Podcast');
  }

  login(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  getImgs() {
    return this.gblAssetsCollection.valueChanges();
  }

  getUser(email: string) {
    const query = this.gblUserCollection.ref.where('email', '==', email);
    return query.get();
  }

  createUser(user: any, uid: any) {
    return this.gblUserCollection.doc(uid).set(user);
  }

  createUserAuth(email){
    return this.afAuth.createUserWithEmailAndPassword(email, '12345678');
  }

  getGrupo() {
    return this.gblGrupoCollection.valueChanges();
  }

  getEstudiosSem() {
    return this.gblSemanalCollection.valueChanges();
  }

  getPodcastsAll() {
    return this.gblPodcastCollection.valueChanges();
  }
  
}
