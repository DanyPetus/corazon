// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCo7WrHtjzxvy1nYUAH_ssFiKr1ETNbf7w",
    authDomain: "corazon-6fd1d.firebaseapp.com",
    projectId: "corazon-6fd1d",
    storageBucket: "corazon-6fd1d.appspot.com",
    messagingSenderId: "457643992144",
    appId: "1:457643992144:web:d12176886398441cae5d55",
    measurementId: "G-Y29SNEBKFX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
